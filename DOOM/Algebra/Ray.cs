﻿using Algebra;
using DOOM.Helpers;
using DOOM.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Doom3D.Algebra
{
    //Луч
    public class Ray
    {
        //Луч задается системой из двух уравнений {x = startX + K * A ; y = startY + K * b}, где a,b - направляющий вектор
        //Точка лежит на луче, если K в обоих уравнениях >= 0
        //Если a=0, то x должен быть равен startX
        //Если b=0, то y должен быть равен startY

        private Coordinate startPoint;
        private double direction;
        private double sinDirection;
        private double cosDirection;
        private double T;//праметр

        public Ray(Coordinate startPoint, double direction)
        {
            this.startPoint = startPoint;
            this.direction = direction;
            cosDirection = Math.Cos(direction);
            sinDirection = Math.Sin(direction);
        }

        public bool IsIntersect(Segment segment)
        {
            //Метод Крамера
            var d = (segment.End.X - segment.Start.X) * sinDirection - (segment.End.Y - segment.Start.Y) * cosDirection;
            var d1 = (segment.End.X - segment.Start.X) * (segment.Start.Y - startPoint.Y) - (segment.Start.X - startPoint.X) * (segment.End.Y - segment.Start.Y);
            var d2 = cosDirection * (segment.Start.Y - startPoint.Y) - sinDirection * (segment.Start.X - startPoint.X);

            if (d == 0) 
                return false;

            T = d1 / d;

            var K = d2 / d;

            return K >= 0 && K <= 1 && T >= 0;
        }

        public bool IsPointOnRay(Coordinate point)
        {
            if(Math.Abs(cosDirection - 0) < 1e-1)
            {
                var t = (point.Y - startPoint.Y) / sinDirection;
                return point.X == startPoint.X && t >= 0;
            }
            if (Math.Abs(sinDirection - 0) < 1e-1)
            {
                var t = (point.X - startPoint.X) / cosDirection;
                return point.Y == startPoint.Y && t >= 0;
            }
            var t1 = (point.X - startPoint.X) / cosDirection;
            var t2 = (point.Y - startPoint.Y) / sinDirection;
            return Math.Abs(t1 - t2) < 1e-1 && t1 >= 0;
        }

        /// <summary>
        /// Возвращает точку пересечения с отрезком.
        /// </summary>
        /// <param name="s - отрезок"></param>
        /// <returns></returns>
        public bool GetIntersectionPoint(Segment s, out Coordinate? intersectionPoint, out double? distanceFromSegmentStartToIntersectionPoint)
        {
            if (!IsIntersect(s))
            {
                intersectionPoint = null;
                distanceFromSegmentStartToIntersectionPoint = null;
                return false;
            }

            intersectionPoint = new Coordinate(startPoint.X + T * cosDirection, startPoint.Y + T * sinDirection);
            distanceFromSegmentStartToIntersectionPoint = AlgebraHelper.GetLengthBeetwenPoints(s.Start, intersectionPoint);
            return true;
        }
    }
}
