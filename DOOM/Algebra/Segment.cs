﻿using Algebra;
using DOOM.Helpers;
using DOOM.Models;
using System;
using System.DirectoryServices;
using System.Drawing;
using System.Reflection;

namespace Doom3D.Algebra
{
    public class Segment
    {
        public Coordinate Start { get; private set; }
        public Coordinate End { get; private set; }
        private double? _Length;
        public double Length 
        { 
            get 
            {
                if (_Length == null)
                    _Length = AlgebraHelper.GetLengthBeetwenPoints(Start, End);

                return _Length.Value; 
            } 
        }

        public Segment(double x1, double y1, double x2, double y2)
        {
            Start = new Coordinate(x1, y1);
            End = new Coordinate(x2, y2);
        }

        public Segment(Coordinate point1, Coordinate point2)
        {
            Start = new Coordinate(point1.X, point1.Y);
            End = new Coordinate(point2.X, point2.Y);
        }

        public bool IsPointOnSegment(Coordinate point)
        {
            var K1 = (point.X - Start.X) / (End.X - Start.X);
            var K2 = (point.Y - Start.Y) / (End.Y - Start.Y);
            return K1 == K2 && K1 >= 0 && K1 <= 1;
        }

        //public PointF GetIntersectionPoint(Line line)
        //{
        //    var k1 = -line.A * X1 - line.B * Y1 - line.C; //числитель
        //    var k2 = -line.A * X1 + line.A * X2 - line.B * Y1 + line.B * Y2;//знаменатель
        //    var K = k1 / k2;
        //    if (!(K >= 0 && K <= 1)) return PointF.Empty;
        //    double X = (1 - K) * X1 + K * X2;
        //    double Y = (1 - K) * Y1 + K * Y2;
        //    return new PointF((float)X, (float)Y);
        //}

        public bool GetIntersectionPoint(Segment another, out Coordinate? intersectionPoint)
        {
            var delta = (this.End.X - this.Start.X) * (-1) * (another.End.Y - another.Start.Y) + (another.End.X - another.Start.X) * (this.End.Y - this.Start.Y);
            
            if(Math.Abs(delta) < 1e-5)
            {
                intersectionPoint = null;
                return false;
            }

            var deltaT1 = (another.Start.X - this.Start.X) * (-1) * (another.End.Y - another.Start.Y) + (another.Start.Y - this.Start.Y) * (another.End.X - another.Start.X);
            var deltaT2 = (this.End.X - this.Start.X) * (another.Start.Y - this.Start.Y) - (this.End.Y - this.Start.Y) * (another.Start.X - this.Start.X);

            var T1 = deltaT1 / delta;
            var T2 = deltaT2 / delta;

            //У отрезков 0 <= T <= 1
            if(T1 < 0 || T1 > 1 || T2 < 0 || T2 > 1)
            {
                intersectionPoint = null;
                return false;
            }

            intersectionPoint = new Coordinate(this.Start.X + (this.End.X - this.Start.X) * T1, this.Start.Y + (this.End.Y - this.Start.Y) * T1);
            return true;
        }
    }
}
