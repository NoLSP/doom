﻿using DOOM.Models;
using System;
using System.Drawing;

namespace Algebra
{
  public  class Vector
    {
        public double X;
        public double Y;
        public double Length { get => Math.Sqrt(X * X + Y * Y); }

        public Vector(float x, float y)
        {
            X = x;
            Y = y;
        }

        public Vector(Coordinate point1, Coordinate point2)
        {
            X = point2.X - point1.X;
            Y = point2.Y - point1.Y;
        }

        public Vector(Coordinate start, double angle)
        {
            X = start.X + Math.Cos(angle);
            Y = start.Y + Math.Sin(angle);
        }

        public Vector Normalize()
        {
            X = X / Length;
            Y = Y / Length;
            return this;
        }
    }
}
