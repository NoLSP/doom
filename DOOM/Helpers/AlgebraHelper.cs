﻿using DOOM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOOM.Helpers
{
    public static class AlgebraHelper
    {
        public static double GetLengthBeetwenPoints(Coordinate p1, Coordinate p2)
        {
            return Math.Sqrt((p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y));
        }
    }
}
