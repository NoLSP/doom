﻿using DOOM.Constants;
using DOOM.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOOM.Helpers
{
    public static class AppDataHelper
    {
        public static List<WallInfo>? GetLevelWallInfos(string fileRelativaPath)
        {
            var fileAbsolutePath = Path.Combine(Paths.LevelsDirectoryAbsolutePath(), fileRelativaPath);
            var fileContentStringArray = File.ReadAllLines(fileAbsolutePath);
            var fileContentString = String.Join("", fileContentStringArray);
            var gameLevelInfo = JsonConvert.DeserializeObject<List<WallInfo>>(fileContentString);
            return gameLevelInfo;
        }

        public static Bitmap GetTexture(string relativePath)
        {
            var imagePath = Path.Combine(Paths.TexturesDirectoryAbsolutePath(), relativePath);
            Bitmap texture = new Bitmap(imagePath);
            return texture;
        }

        public static Bitmap GetMainMenuBacground()
        {
            return new Bitmap(Path.Combine(Paths.MenuResourcesDirectoryAbsolutePath(), "Background.jpg"));
        }

        public static Bitmap GetMainMenuTitleImage()
        {
            return new Bitmap(Path.Combine(Paths.MenuResourcesDirectoryAbsolutePath(), "TitleImage.png"));
        }

        public static Bitmap GetMainMenuButtonBackground()
        {
            return new Bitmap(Path.Combine(Paths.MenuResourcesDirectoryAbsolutePath(), "ButtonBackground.png"));
        }

        public static Bitmap GetMainMenuButtonHoverBackground()
        {
            return new Bitmap(Path.Combine(Paths.MenuResourcesDirectoryAbsolutePath(), "ButtonBackground_hover.png"));
        }

        public static Bitmap GetMainMenuLevelBackgroundHover()
        {
            return new Bitmap(Path.Combine(Paths.MenuResourcesDirectoryAbsolutePath(), "levelcard_background_hover.png"));
        }

        public static Bitmap GetMainMenuLevelBackground()
        {
            return new Bitmap(Path.Combine(Paths.MenuResourcesDirectoryAbsolutePath(), "levelcard_background.png"));
        }

        public static Bitmap GetPlayerPanel()
        {
            return new Bitmap(Path.Combine(Paths.MenuResourcesDirectoryAbsolutePath(), "player_panel.png"));
        }
    }
}
