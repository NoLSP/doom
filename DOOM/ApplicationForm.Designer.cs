﻿using DOOM.Helpers;
using DOOM.Models;

namespace DOOM
{
    partial class ApplicationForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        #region Main Menu

        private static Image BackgroundImage;
        private static Image ButtonBackgroundImage;
        private static Image ButtonBackgroundHoverImage;
        private static Image TitleImage;

        private static Panel MainMenuPagePanel;
        private static Button MainMenuPlayButton;
        private static PictureBox MainMenuBackgroundPictureBox;
        private static PictureBox MainMenuTitlePictureBox;

        private static Panel GetMainMenuPanel(Size size)
        {
            MainMenuPagePanel = new Panel();
            MainMenuPagePanel.Size = size;
            MainMenuPagePanel.Location = new Point(0, 0);

            BackgroundImage = AppDataHelper.GetMainMenuBacground();
            ButtonBackgroundImage = AppDataHelper.GetMainMenuButtonBackground();
            ButtonBackgroundHoverImage = AppDataHelper.GetMainMenuButtonHoverBackground();
            TitleImage = AppDataHelper.GetMainMenuTitleImage();

            MainMenuBackgroundPictureBox = new PictureBox();
            MainMenuBackgroundPictureBox.ClientSize = size;
            MainMenuBackgroundPictureBox.Location = new Point(0, 0);
            MainMenuBackgroundPictureBox.Image = BackgroundImage;

            MainMenuTitlePictureBox = new PictureBox();
            MainMenuTitlePictureBox.Size = new Size(373, 212);
            MainMenuTitlePictureBox.Location = new Point(313, 20);
            MainMenuTitlePictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
            MainMenuTitlePictureBox.BackColor = Color.Transparent;
            MainMenuTitlePictureBox.BackgroundImage = TitleImage;
            MainMenuBackgroundPictureBox.Controls.Add(MainMenuTitlePictureBox);

            MainMenuPlayButton = new Button();
            MainMenuPlayButton.Size = new Size(250, 67);
            MainMenuPlayButton.Location = new Point(375, 350);
            MainMenuPlayButton.BackgroundImage = ButtonBackgroundImage;
            MainMenuPlayButton.BackgroundImageLayout = ImageLayout.Stretch;
            MainMenuPlayButton.BackColor = Color.Transparent;
            MainMenuPlayButton.FlatStyle = FlatStyle.Flat;
            MainMenuPlayButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
            MainMenuPlayButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
            MainMenuPlayButton.FlatAppearance.BorderSize = 0;
            MainMenuPlayButton.Font = new Font(new FontFamily("Tahoma"), 15F, FontStyle.Bold);
            MainMenuPlayButton.ForeColor = Color.LightSalmon;
            MainMenuPlayButton.Text = "Играть";
            MainMenuPlayButton.Click += MainMenuPlayButtonClicked;
            MainMenuPlayButton.MouseEnter += MainMenuPlayButton_MouseEnter;
            MainMenuPlayButton.MouseLeave += MainMenuPlayButton_MouseLeave;
            MainMenuBackgroundPictureBox.Controls.Add(MainMenuPlayButton);
            

            MainMenuPagePanel.Controls.Add(MainMenuBackgroundPictureBox);
            MainMenuPagePanel.Controls.SetChildIndex(MainMenuBackgroundPictureBox, 3);
            //MainMenuPagePanel.Controls.Add(MainMenuPlayButton);
            //MainMenuPagePanel.Controls.SetChildIndex(MainMenuPlayButton, 0);

            return MainMenuPagePanel;
        }

        private static void MainMenuPlayButton_MouseLeave(object sender, EventArgs e)
        {
            (sender as Button).BackgroundImage = ButtonBackgroundImage;
            (sender as Button).ForeColor = Color.LightSalmon;
        }

        private static void MainMenuPlayButton_MouseEnter(object sender, EventArgs e)
        {
            (sender as Button).BackgroundImage = ButtonBackgroundHoverImage;
            (sender as Button).ForeColor = Color.RosyBrown;
        }

        #endregion

        #region Levels

        private static Panel LevelsPagePanel;
        private static Image LevelCardBackgroundImage;
        private static Image LevelCardBackgroundHoverImage;
        private static PictureBox LevelsBackgroundPictureBox;
        private static PictureBox LevelsTitlePictureBox;
        private static List<Panel> LevelPanels;

        private static Panel GetLevelsPanel(Size size, List<Game> games)
        {
            LevelCardBackgroundImage = AppDataHelper.GetMainMenuLevelBackground();
            LevelCardBackgroundHoverImage = AppDataHelper.GetMainMenuLevelBackgroundHover();

            LevelsPagePanel = new Panel();
            LevelsPagePanel.Size = size;
            LevelsPagePanel.Location = new Point(0, 0);

            LevelsBackgroundPictureBox = new PictureBox();
            LevelsBackgroundPictureBox.ClientSize = size;
            LevelsBackgroundPictureBox.Location = new Point(0, 0);
            LevelsBackgroundPictureBox.Image = BackgroundImage;

            LevelsTitlePictureBox = new PictureBox();
            LevelsTitlePictureBox.Size = new Size(373, 212);
            LevelsTitlePictureBox.Location = new Point(313, 20);
            LevelsTitlePictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
            LevelsTitlePictureBox.BackColor = Color.Transparent;
            LevelsTitlePictureBox.BackgroundImage = AppDataHelper.GetMainMenuTitleImage();
            LevelsBackgroundPictureBox.Controls.Add(LevelsTitlePictureBox);

            LevelPanels = new List<Panel>();
            var heightOffset = 0;
            foreach(var gameLevel in games)
            {
                var levelPanel = new Panel();
                levelPanel.Size = new Size(500, 150);
                levelPanel.Name = $"{gameLevel.Id}";
                levelPanel.BackColor = Color.Transparent;
                levelPanel.MouseLeave += LevelPanel_MouseLeave;
                levelPanel.BorderStyle= BorderStyle.FixedSingle;
                levelPanel.Location = new Point(250, 250 + heightOffset);

                var levelBackgroundPictureBox = new PictureBox();
                levelBackgroundPictureBox.Name = $"{gameLevel.Id}";
                levelBackgroundPictureBox.Size = levelPanel.Size;
                levelBackgroundPictureBox.Location = new Point(0, 0);
                levelBackgroundPictureBox.BackgroundImage = LevelCardBackgroundImage;
                levelBackgroundPictureBox.BackgroundImageLayout = ImageLayout.Stretch;
                levelBackgroundPictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
                levelBackgroundPictureBox.BackColor = Color.Transparent;
                levelBackgroundPictureBox.MouseEnter += LevelBackgroundPictureBox_MouseEnter;
                levelBackgroundPictureBox.MouseLeave += LevelBackgroundPictureBox_MouseLeave;
                levelBackgroundPictureBox.MouseClick += LevelsPanel_MouseClick;
                levelPanel.Controls.Add(levelBackgroundPictureBox);

                var levelTitleLabel = new Label();
                levelTitleLabel.Size = new Size(500, 30);
                levelTitleLabel.Location = new Point(0, 25);
                levelTitleLabel.Font = new Font(new FontFamily("ManEater BB"), 18F, FontStyle.Bold);
                levelTitleLabel.ForeColor = Color.LightSalmon;
                levelTitleLabel.BackColor = Color.Transparent;
                levelTitleLabel.Text = gameLevel.Title;
                levelTitleLabel.TextAlign = ContentAlignment.TopCenter;
                levelTitleLabel.MouseEnter += LevelTitleLabel_MouseEnter;
                levelTitleLabel.MouseLeave += LevelTitleLabel_MouseLeave;
                levelTitleLabel.Click += LevelTitleLabel_Click;
                levelBackgroundPictureBox.Controls.Add(levelTitleLabel);

                var levelDescriptionLabel = new Label();
                levelDescriptionLabel.Size = new Size(500, 70);
                levelDescriptionLabel.Location = new Point(0, 70);
                levelDescriptionLabel.Font = new Font(new FontFamily("ManEater BB"), 12F, FontStyle.Bold);
                levelDescriptionLabel.ForeColor = Color.LightSalmon;
                levelDescriptionLabel.BackColor = Color.Transparent;
                levelDescriptionLabel.Text = gameLevel.Description;
                levelDescriptionLabel.TextAlign = ContentAlignment.TopCenter;
                levelDescriptionLabel.MouseEnter += LevelDescriptionLabel_MouseEnter;
                levelDescriptionLabel.MouseLeave += LevelDescriptionLabel_MouseLeave;
                levelDescriptionLabel.Click += LevelDescriptionLabel_Click;
                levelBackgroundPictureBox.Controls.Add(levelDescriptionLabel);

                LevelsBackgroundPictureBox.Controls.Add(levelPanel);

                heightOffset += 210;
            }

            LevelsPagePanel.Controls.Add(LevelsBackgroundPictureBox);
            LevelsPagePanel.Controls.SetChildIndex(LevelsBackgroundPictureBox, 3);


            return LevelsPagePanel;
        }

        private static void LevelDescriptionLabel_Click(object sender, EventArgs e)
        {
            LevelsPanel_MouseClick((sender as Label).Parent as PictureBox, null);
        }

        private static void LevelTitleLabel_Click(object sender, EventArgs e)
        {
            LevelsPanel_MouseClick((sender as Label).Parent as PictureBox, null);
        }

        private static void LevelDescriptionLabel_MouseLeave(object sender, EventArgs e)
        {
            LevelBackgroundPictureBox_MouseLeave((sender as Label).Parent as PictureBox, null);
        }

        private static void LevelTitleLabel_MouseLeave(object sender, EventArgs e)
        {
            LevelBackgroundPictureBox_MouseLeave((sender as Label).Parent as PictureBox, null);
        }

        private static void LevelDescriptionLabel_MouseEnter(object sender, EventArgs e)
        {
            ((sender as Label).Parent as PictureBox).BackgroundImage = LevelCardBackgroundHoverImage;
        }

        private static void LevelTitleLabel_MouseEnter(object sender, EventArgs e)
        {
            ((sender as Label).Parent as PictureBox).BackgroundImage = LevelCardBackgroundHoverImage;
        }

        private static void LevelPreviewPictureBox_MouseEnter(object sender, EventArgs e)
        {
            ((sender as PictureBox).Parent as PictureBox).BackgroundImage = LevelCardBackgroundHoverImage;
        }

        private static void LevelBackgroundPictureBox_MouseLeave(object sender, EventArgs e)
        {
            (sender as PictureBox).BackgroundImage = LevelCardBackgroundImage;
        }

        private static void LevelPanel_MouseLeave(object sender, EventArgs e)
        {
            (sender as Panel).Controls.Find((sender as Panel).Name, false)[0].BackgroundImage = LevelCardBackgroundImage;
        }

        private static void LevelBackgroundPictureBox_MouseEnter(object sender, EventArgs e)
        {
            (sender as PictureBox).BackgroundImage = LevelCardBackgroundHoverImage;
        }

        #endregion

        #region Game

        private static Panel GamePagePanel;
        private static PictureBox GamePictureBox;
        private static Label GameGoalsLabel;
        private static Label GameLoseLabel;
        private static Label GameWinLabel;
        private static PictureBox GameBackgroundPictureBox;
        private static PictureBox GamePlayerPanelPictureBox;
        public static Label GamePlayerHealthLabel;
        private static Label GamePlayerAmmoLabel;
        private static Label GamePlayerArmorLabel;
        private static Label GamePlayerArm1Label;
        private static Label GamePlayerArm2Label;
        private static Label GamePlayerArm3Label;
        private static Label GamePlayerArm4Label;
        private static Label GamePlayerArm5Label;
        private static Label GamePlayerArm6Label;


        private static Panel GetGamePictureBox(Size size)
        {
            GamePagePanel = new Panel();
            GamePagePanel.Size = size;
            GamePagePanel.Location = new Point(0, 0);

            GameBackgroundPictureBox = new PictureBox();
            GameBackgroundPictureBox.ClientSize = size;
            GameBackgroundPictureBox.Location = new Point(0, 0);
            GameBackgroundPictureBox.Image = BackgroundImage;

            GamePictureBox = new PictureBox();
            GamePictureBox.Size = new Size(1000, 650);
            GamePictureBox.Location = new Point(0, -10);

            GamePlayerPanelPictureBox = new PictureBox();
            GamePlayerPanelPictureBox.ClientSize = new Size(1000, 130);
            GamePlayerPanelPictureBox.Location = new Point(0, 630);
            GamePlayerPanelPictureBox.Image = AppDataHelper.GetPlayerPanel();
            GamePlayerPanelPictureBox.SizeMode = PictureBoxSizeMode.StretchImage;

            GamePlayerHealthLabel = new Label();
            GamePlayerHealthLabel.Size = new Size(200, 100);
            GamePlayerHealthLabel.Location = new Point(145, 0);
            GamePlayerHealthLabel.BackColor = Color.Transparent;
            GamePlayerHealthLabel.ForeColor = Color.DarkRed;
            GamePlayerHealthLabel.TextAlign = ContentAlignment.MiddleCenter;
            GamePlayerHealthLabel.Font = new Font(new FontFamily("ManEater BB"), 40F, FontStyle.Bold);
            GamePlayerHealthLabel.Text = "100%";
            GamePlayerPanelPictureBox.Controls.Add(GamePlayerHealthLabel);

            GamePlayerAmmoLabel = new Label();
            GamePlayerAmmoLabel.Size = new Size(150, 100);
            GamePlayerAmmoLabel.Location = new Point(0, 0);
            GamePlayerAmmoLabel.BackColor = Color.Transparent;
            GamePlayerAmmoLabel.ForeColor = Color.DarkRed;
            GamePlayerAmmoLabel.TextAlign = ContentAlignment.MiddleCenter;
            GamePlayerAmmoLabel.Font = new Font(new FontFamily("ManEater BB"), 40F, FontStyle.Bold);
            GamePlayerAmmoLabel.Text = "100";
            GamePlayerPanelPictureBox.Controls.Add(GamePlayerAmmoLabel);

            GamePlayerArm1Label = new Label();
            GamePlayerArm1Label.Size = new Size(50, 50);
            GamePlayerArm1Label.Location = new Point(325, 0);
            GamePlayerArm1Label.BackColor = Color.Transparent;
            GamePlayerArm1Label.ForeColor = Color.GreenYellow;
            GamePlayerArm1Label.TextAlign = ContentAlignment.MiddleCenter;
            GamePlayerArm1Label.Font = new Font(new FontFamily("ManEater BB"), 20F, FontStyle.Bold);
            GamePlayerArm1Label.Text = "1";
            GamePlayerPanelPictureBox.Controls.Add(GamePlayerArm1Label);

            GamePlayerArm2Label = new Label();
            GamePlayerArm2Label.Size = new Size(50, 50);
            GamePlayerArm2Label.Location = new Point(365, 0);
            GamePlayerArm2Label.BackColor = Color.Transparent;
            GamePlayerArm2Label.ForeColor = Color.Gray;
            GamePlayerArm2Label.TextAlign = ContentAlignment.MiddleCenter;
            GamePlayerArm2Label.Font = new Font(new FontFamily("ManEater BB"), 20F, FontStyle.Bold);
            GamePlayerArm2Label.Text = "2";
            GamePlayerPanelPictureBox.Controls.Add(GamePlayerArm2Label);

            GamePlayerArm3Label = new Label();
            GamePlayerArm3Label.Size = new Size(50, 50);
            GamePlayerArm3Label.Location = new Point(402, 0);
            GamePlayerArm3Label.BackColor = Color.Transparent;
            GamePlayerArm3Label.ForeColor = Color.Gray;
            GamePlayerArm3Label.TextAlign = ContentAlignment.MiddleCenter;
            GamePlayerArm3Label.Font = new Font(new FontFamily("ManEater BB"), 20F, FontStyle.Bold);
            GamePlayerArm3Label.Text = "3";
            GamePlayerPanelPictureBox.Controls.Add(GamePlayerArm3Label);

            GamePlayerArm4Label = new Label();
            GamePlayerArm4Label.Size = new Size(50, 50);
            GamePlayerArm4Label.Location = new Point(325, 40);
            GamePlayerArm4Label.BackColor = Color.Transparent;
            GamePlayerArm4Label.ForeColor = Color.Gray;
            GamePlayerArm4Label.TextAlign = ContentAlignment.MiddleCenter;
            GamePlayerArm4Label.Font = new Font(new FontFamily("ManEater BB"), 20F, FontStyle.Bold);
            GamePlayerArm4Label.Text = "4";
            GamePlayerPanelPictureBox.Controls.Add(GamePlayerArm4Label);

            GamePlayerArm5Label = new Label();
            GamePlayerArm5Label.Size = new Size(50, 50);
            GamePlayerArm5Label.Location = new Point(365, 40);
            GamePlayerArm5Label.BackColor = Color.Transparent;
            GamePlayerArm5Label.ForeColor = Color.Gray;
            GamePlayerArm5Label.TextAlign = ContentAlignment.MiddleCenter;
            GamePlayerArm5Label.Font = new Font(new FontFamily("ManEater BB"), 20F, FontStyle.Bold);
            GamePlayerArm5Label.Text = "5";
            GamePlayerPanelPictureBox.Controls.Add(GamePlayerArm5Label);

            GamePlayerArm6Label = new Label();
            GamePlayerArm6Label.Size = new Size(50, 50);
            GamePlayerArm6Label.Location = new Point(402, 40);
            GamePlayerArm6Label.BackColor = Color.Transparent;
            GamePlayerArm6Label.ForeColor = Color.Gray;
            GamePlayerArm6Label.TextAlign = ContentAlignment.MiddleCenter;
            GamePlayerArm6Label.Font = new Font(new FontFamily("ManEater BB"), 20F, FontStyle.Bold);
            GamePlayerArm6Label.Text = "6";
            GamePlayerPanelPictureBox.Controls.Add(GamePlayerArm6Label);

            GameGoalsLabel = new Label();
            GameGoalsLabel.Size = new Size(250, 150);
            GameGoalsLabel.Location = new Point(14, 300);
            GameGoalsLabel.BackColor = Color.Transparent;
            GameGoalsLabel.ForeColor = Color.White;
            GameGoalsLabel.TextAlign = ContentAlignment.TopLeft;
            GameGoalsLabel.Font = new Font(new FontFamily("ManEater BB"), 12F, FontStyle.Regular);
            GamePictureBox.Controls.Add(GameGoalsLabel);

            GameLoseLabel = new Label();
            GameLoseLabel.Size = new Size(800, 300);
            GameLoseLabel.Location = new Point(130, 350);
            GameLoseLabel.BackColor = Color.Transparent;
            GameLoseLabel.ForeColor = Color.DarkRed;
            GameLoseLabel.TextAlign = ContentAlignment.MiddleCenter;
            GameLoseLabel.Text = "Game over!";
            GameLoseLabel.Font = new Font(new FontFamily("ManEater BB"), 80F, FontStyle.Regular);

            GameWinLabel = new Label();
            GameWinLabel.Size = new Size(600, 300);
            GameWinLabel.Location = new Point(200, 350);
            GameWinLabel.BackColor = Color.Transparent;
            GameWinLabel.ForeColor = Color.Green;
            GameWinLabel.TextAlign = ContentAlignment.MiddleCenter;
            GameWinLabel.Text = "Yuo win!";
            GameWinLabel.Font = new Font(new FontFamily("ManEater BB"), 80F, FontStyle.Bold);

            GamePagePanel.Controls.Add(GameBackgroundPictureBox);
            GamePagePanel.Controls.Add(GamePictureBox);
            GamePagePanel.Controls.Add(GamePlayerPanelPictureBox);
            GamePagePanel.Controls.SetChildIndex(GameBackgroundPictureBox, 3);

            return GamePagePanel;
        }

        #endregion



        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ApplicationForm));
            this.SuspendLayout();
            // 
            // ApplicationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 750);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "ApplicationForm";
            this.Text = "DOOM";
            this.ResumeLayout(false);

        }

        #endregion
    }
}