﻿using Doom3D.Algebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOOM.Models
{
    public class Wall
    {
        public Segment Segment { get; set; }
        public Bitmap Texture { get; set; } // текстура 
        public Dictionary<int, Image> TextureParts;

        public Wall(Segment segment, Bitmap texture)
        {
            Segment = segment;
            Texture = texture;

            TextureParts =  new Dictionary<int, Image>();
            for (int i = 0; i < texture.Width; i++)
            {
                var image = new Bitmap(1, texture.Height);
                var g = Graphics.FromImage(image);
                g.DrawImage(texture, new Rectangle(0, 0, 1, texture.Height), new Rectangle(i, 0, 1, texture.Height), GraphicsUnit.Pixel);
                TextureParts.Add(i, image);
            }
        }
    }
}
