﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOOM.Models
{
    public class MobInfo
    {
        public int ColumnNumber { get; set; }
        public double Distance { get; set; }
        public Mob Mob{ get; set; }
    }
}
