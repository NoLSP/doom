﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace DOOM.Models
{
    public class Mob
    {
        public int Id { get; set; }
        public int MaxHealthValue { get; set; } // максимальный уровень здоровья
        public int HealthValue { get; set; } // текущее значение здоровья
        public int DamageValue { get; set; } // величина наносимого урона 
        public int DamageCooldownMilliseconds { get; set; } // перезарядка
        public DateTime? LastShotDateTime { get; set; }
        public Bitmap Texture { get; set; } // текстура
        public double StepValue { get; set; } // велечина шага
        public Coordinate Coordinate { get; set; }
        public MediaPlayer SoundDeath { get; set; }

        public Mob() { }

        public Mob(int maxHealthValue, int healthValue, int damageValue, int damageCooldown, Bitmap texture, double stepValue) 
        {
            MaxHealthValue = maxHealthValue;
            HealthValue = healthValue;
            DamageValue = damageValue;
            DamageCooldownMilliseconds = damageCooldown;
            Texture = texture;
            StepValue = stepValue;
        }

        public void Move(List<Wall> walls, Coordinate playerCoordinate)
        {

        }
    }
}
