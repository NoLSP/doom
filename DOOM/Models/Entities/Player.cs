﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace DOOM.Models
{
    public class Player
    {
        public double Direction { get; set; } // направление(куда смотрит) в радианах
        public double ViewAngle { get; set; } // угол обзора в радианах
        public Coordinate Coordinate { get; set; } // координата
        public int HealthValue { get; set; } // уровень здоровья
        public int MaxHealthValue { get; set; } // максимальный уровень здоровья
        public double StepValue { get; set; }
        public double StepCamera { get; set; }
        public Weapon CurrentWeapon { get; set; }
        public List<Weapon> Weapons { get; set; } // оружие
        public List<AIDKit> AIDkits { get; set; } // аптечка
        public MediaPlayer SoundDeath { get; set; }

        public Player() { }
    
        public Player(double direction, double viewAngle, Coordinate coordinate, int healthValue, int maxHealthValue, List<Weapon> weapons, List<AIDKit> aidKits)
        {
            Direction = direction;
            ViewAngle = viewAngle;
            StepValue = 0.1;
            StepCamera = Math.PI / 40;
            Coordinate = coordinate;
            HealthValue = healthValue;
            MaxHealthValue = maxHealthValue;
            Weapons = weapons;
            AIDkits = aidKits;
        }
    }
}
