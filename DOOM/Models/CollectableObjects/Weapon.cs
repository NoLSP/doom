﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace DOOM.Models
{
    public class Weapon
    {
        public WeaponType Type { get; set; } // тип оружия 
        public string Title { get; set; } // название оружия
        public int BulletsCount { get; set; } // количество патронов 
        public int DamageValue { get; set; } // величина наносимого урона 
        public int DamageCooldown { get; set; } // перезарядка
        public CollectableObjectPosition Position { get; set; } // положение оружия(на карте / у игрока)
        public Coordinate? Coordinate { get; set; } // координата
        public Bitmap Texture { get; set; } // текстура 
        public Bitmap ShotTexture { get; set; } // текстура
        public MediaPlayer SoundShoot { get; set; }

        public static string ObtainWeaponTitle(WeaponType type)
        {
            switch(type)
            {
                case WeaponType.Rifle:
                    return "Винтовка";
                case WeaponType.Gun:
                    return "Пистолет";
                case WeaponType.ShotGun:
                    return "Дробовик";
            }

            throw new Exception("Не удалось определить название оружия");
        }

        public Weapon() { }

        public Weapon(WeaponType type, int bulletsCount, int damageValue, int damageCooldown, CollectableObjectPosition position, Coordinate? coordinate, Bitmap texture)
        {
            Type = type;
            Title = ObtainWeaponTitle(type);
            BulletsCount = bulletsCount;
            DamageValue = damageValue;
            DamageCooldown = damageCooldown;
            Position = position;
            Coordinate = coordinate;
            Texture = texture;
        }
    }
}
