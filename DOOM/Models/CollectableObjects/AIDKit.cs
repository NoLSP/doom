﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOOM.Models
{
    public class AIDKit
    {
        public int RecoverableHealthValue { get; set; } // восстанавливаемое значение здоровья
        public CollectableObjectPosition Position { get; set; } // положение оружия(на карте / у игрока)
        public Coordinate? Coordinate { get; set; } // координата 
        public Bitmap Texture { get; set; } // текстура 

        public AIDKit() { }

        public AIDKit(int recoverableHealthValue, CollectableObjectPosition position, Coordinate? coordinate, Bitmap texture) 
        { 
            RecoverableHealthValue = recoverableHealthValue;
            Position = position;
            Coordinate = coordinate;
            Texture = texture;
        }
    }
}
