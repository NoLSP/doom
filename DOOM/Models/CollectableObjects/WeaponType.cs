﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOOM.Models
{
    public enum WeaponType
    {
        Rifle = 1, //Винтовка
        Gun = 2, //Пистолет
        ShotGun = 3 //Дробовик
    }
}
