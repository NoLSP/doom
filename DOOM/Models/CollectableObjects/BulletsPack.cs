﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOOM.Models
{
    public class BulletsPack
    {
        public WeaponType WeaponType { get; set; } // тип оружия 
        public int Count { get; set; } // количество патронов
        public Coordinate Coordinate { get; set; } // координата 
        public Bitmap Texture { get; set; } // текстура
                                           
        public BulletsPack() { }

        public BulletsPack(WeaponType weaponType, int count, Coordinate coordinate, Bitmap texture) 
        { 
            WeaponType = weaponType;
            Count = count;
            Coordinate = coordinate;
            Texture = texture;
        }
    }
}
