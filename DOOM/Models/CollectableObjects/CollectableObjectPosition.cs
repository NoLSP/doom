﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOOM.Models
{
    public enum CollectableObjectPosition
    {
        OnMap = 1, // на карте
        Player = 2 // у игрока 
    }
}
