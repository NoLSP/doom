﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOOM.Models
{
    public class WallInfo
    {
        public Coordinate? StartCoordinate { get; set; }
        public Coordinate? EndCoordinate { get; set; }
        public string? TextureRelativePath { get; set; }
    }
}
