﻿using DOOM.Models;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace DOOM
{
    public partial class ApplicationForm : Form
    {
        public static ApplicationForm instance;
        private System.Windows.Forms.Timer timer;

        public ApplicationForm()
        {

            InitializeComponent();

            var mainMenuPanel = GetMainMenuPanel(this.ClientSize);

            this.Controls.Add(mainMenuPanel);

            GameManager.LoadLevels();

            instance = this;
        }

        #region MainMenu

        public static void MainMenuPlayButtonClicked(object sender, EventArgs e)
        {
            instance.Controls.Clear();
            instance.Controls.Add(GetLevelsPanel(instance.ClientSize, GameManager.Games));
        }

        private static void LevelsPanel_MouseClick(object sender, MouseEventArgs e)
        {
            var control = sender as PictureBox;
            var id = int.Parse(control!.Name);
            GameManager.SetGame(id);

            instance.Controls.Clear();
            instance.Controls.Add(GetGamePictureBox(instance.ClientSize));
            instance.Focus();

            instance.timer = new System.Windows.Forms.Timer();
            instance.timer.Interval = 33;
            instance.timer.Tick += Timer_Tick;
            instance.timer.Start();
        }

        private static void Timer_Tick(object? sender, EventArgs e)
        {
            GameManager.UpdateEntities();
            var goals = GameManager.GetGameGoals();
            var goalsText = "";
            foreach ( var goal in goals)
            {
                if (goalsText.Length != 0)
                    goalsText += "\n";

                goalsText += goal.Title + " - " + (goal.IsComplete ? "✓" : "✖");
            }
            GameGoalsLabel.Text = goalsText;
            GamePictureBox.Image = GameManager.GetFrame();
            if (!goals.Any(x => !x.IsComplete))
            {
                GamePictureBox.Controls.Add(GameWinLabel);
                GameManager.GameComplete();
                instance.timer.Stop();
            }

            if(GameManager.IsGameOver())
            {
                GamePictureBox.Controls.Add(GameLoseLabel);
                GameManager.GameOver();
                instance.timer.Stop();
            }
        }

        #endregion

        protected override void OnKeyDown(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.W:
                    GameManager.MovingPlayerForwardOn();
                    break;
                case Keys.S:
                    GameManager.MovingPlayerBackwardOn();
                    break;
                case Keys.A:
                    GameManager.MovingPlayerLeftOn();
                    break;
                case Keys.D:
                    GameManager.MovingPlayerRightOn();
                    break;
                case Keys.Left:
                    GameManager.RotatePlayerLeftOn();
                    break;
                case Keys.Right:
                    GameManager.RotateCameraRightOn();
                    break;
                case Keys.Space:
                    if(GameManager.PlayerShot(out var ammo))
                        GamePlayerAmmoLabel.Text = ammo.ToString();
                    break;
                case Keys.D1:
                    if (GameManager.SetWeapon(0, out ammo))
                    {
                        ClearArms();
                        GamePlayerArm1Label.ForeColor = Color.GreenYellow;
                        GamePlayerAmmoLabel.Text = ammo.ToString();
                    }
                    break;
                case Keys.D2:
                    if (GameManager.SetWeapon(1, out ammo))
                    {
                        ClearArms();
                        GamePlayerArm2Label.ForeColor = Color.GreenYellow;
                        GamePlayerAmmoLabel.Text = ammo.ToString();
                    }
                    break;
                case Keys.D3:
                    if (GameManager.SetWeapon(2, out ammo))
                    {
                        ClearArms();
                        GamePlayerArm3Label.ForeColor = Color.GreenYellow;
                        GamePlayerAmmoLabel.Text = ammo.ToString();
                    }
                    break;
                case Keys.D4:
                    if (GameManager.SetWeapon(3, out ammo))
                    {
                        ClearArms();
                        GamePlayerArm4Label.ForeColor = Color.GreenYellow;
                        GamePlayerAmmoLabel.Text = ammo.ToString();
                    }
                    break;
                case Keys.D5:
                    if (GameManager.SetWeapon(4, out ammo))
                    {
                        ClearArms();
                        GamePlayerArm5Label.ForeColor = Color.GreenYellow;
                        GamePlayerAmmoLabel.Text = ammo.ToString();
                    }
                    break;
                case Keys.D6:
                    if (GameManager.SetWeapon(5, out ammo))
                    {
                        ClearArms();
                        GamePlayerArm5Label.ForeColor = Color.GreenYellow;
                        GamePlayerAmmoLabel.Text = ammo.ToString();
                    }
                    break;
            }
        }

        protected override void OnKeyUp(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Space:
                    //todo
                    break;
                case Keys.W:
                    GameManager.MovingPlayerForwardOff();
                    break;
                case Keys.S:
                    GameManager.MovingPlayerBackwardOff();
                    break;
                case Keys.A:
                    GameManager.MovingPlayerLeftOff();
                    break;
                case Keys.D:
                    GameManager.MovingPlayerRightOff();
                    break;
                case Keys.Left:
                    GameManager.RotatePlayerLeftOff();
                    break;
                case Keys.Right:
                    GameManager.RotateCameraRightOff();
                    break;
            }
        }

        private void ClearArms()
        {
            GamePlayerArm1Label.ForeColor = Color.Gray;
            GamePlayerArm2Label.ForeColor = Color.Gray;
            GamePlayerArm3Label.ForeColor = Color.Gray;
            GamePlayerArm4Label.ForeColor = Color.Gray;
            GamePlayerArm5Label.ForeColor = Color.Gray;
            GamePlayerArm6Label.ForeColor = Color.Gray;
        }
    }
}