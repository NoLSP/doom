﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOOM.Constants
{
    public static class Paths
    {
        public const string ResourcesDirectoryRelativePath = "Resources";
        public const string LevelsDirectoryRelativePath = "Resources\\Levels";
        public const string TexturesDirectoryRelativePath = "Resources\\Textures";
        public const string MenuResourcesDirectoryRelativePath = "Resources\\Menu";
        public const string SoundsDirectoryRelativePath = "Resources\\Sounds";
        public const string SoundsBackgroundsDirectoryRelativePath = "Resources\\Sounds\\Backgrounds";
        public const string SoundsWeaponsDirectoryRelativePath = "Resources\\Sounds\\Weapons";
        public const string SoundsMobsDirectoryRelativePath = "Resources\\Sounds\\Mobs";
        public const string SoundsPlayerDirectoryRelativePath = "Resources\\Sounds\\Player";

        public static string GetAbsolutePath(string relativePath)
        {
            return Path.Combine(Directory.GetCurrentDirectory(), relativePath);
        }

        public static string ResourcesDirectoryAbsolutePath()
        {
            return GetAbsolutePath(ResourcesDirectoryRelativePath);
        }

        public static string LevelsDirectoryAbsolutePath()
        {
            return GetAbsolutePath(LevelsDirectoryRelativePath);
        }

        public static string TexturesDirectoryAbsolutePath()
        {
            return GetAbsolutePath(TexturesDirectoryRelativePath);
        }

        public static string MenuResourcesDirectoryAbsolutePath()
        {
            return GetAbsolutePath(MenuResourcesDirectoryRelativePath);
        }

        public static string SoundsDirectoryAbsolutePath()
        {
            return GetAbsolutePath(SoundsDirectoryRelativePath);
        }

        public static string GetSoundAbsolutePath(string fileName)
        {
            return Path.Combine(SoundsDirectoryAbsolutePath(), fileName);
        }

        public static string SoundsBackgroundsDirectoryAbsolutePath()
        {
            return GetAbsolutePath(SoundsBackgroundsDirectoryRelativePath);
        }

        public static string GetBackgroundSoundAbsolutePath(string fileName)
        {
            return Path.Combine(SoundsBackgroundsDirectoryAbsolutePath(), fileName);
        }

        public static string SoundsWeaponsDirectoryAbsolutePath()
        {
            return GetAbsolutePath(SoundsWeaponsDirectoryRelativePath);
        }

        public static string GetWeaponsSoundAbsolutePath(string fileName)
        {
            return Path.Combine(SoundsWeaponsDirectoryAbsolutePath(), fileName);
        }

        public static string SoundsMobsDirectoryAbsolutePath()
        {
            return GetAbsolutePath(SoundsMobsDirectoryRelativePath);
        }

        public static string GetMobsSoundAbsolutePath(string fileName)
        {
            return Path.Combine(SoundsMobsDirectoryAbsolutePath(), fileName);
        }

        public static string SoundsPlayerDirectoryAbsolutePath()
        {
            return GetAbsolutePath(SoundsPlayerDirectoryRelativePath);
        }

        public static string GetPlayerSoundAbsolutePath(string fileName)
        {
            return Path.Combine(SoundsPlayerDirectoryAbsolutePath(), fileName);
        }        
    }
}
