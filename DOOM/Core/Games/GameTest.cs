﻿using Algebra;
using DOOM.Constants;
using DOOM.Helpers;
using DOOM.Models;
using Doom3D.Algebra;
using System.Media;
using System.Numerics;
using System.Security.Policy;
using System.Windows.Media;

namespace DOOM
{
    public class GameTest : Game
    {
        private List<Coordinate> MobsSpawnPoints;
        private DateTime LastMobSpawnDateTime;

        #region Initialization

        public GameTest(List<Wall> walls) 
        {
            Id = 1;
            Title = "Тестовый уровень";
            Description = "Уровень для тестов";
            Walls = walls;
            Mobs = InitializeMobs();
            AIDKits = InitializeAIDKits();
            Weapons = InitializeWeapons();
            var playerWeapons = Weapons.Where(x => x.Position == CollectableObjectPosition.Player).ToList();
            BulletsPacks = InitializeBulletsPacks();
            Player = new Player
            {
                Direction = -Math.PI,
                ViewAngle = Math.PI / 3,
                StepValue = 0.2,
                StepCamera = Math.PI / 30,
                Coordinate = new Coordinate(8, 2),
                HealthValue = 100,
                MaxHealthValue = 100,
                AIDkits = new List<AIDKit>(),
                Weapons = playerWeapons,
                CurrentWeapon = playerWeapons[0]
            };

            MobsSpawnPoints = new List<Coordinate>()
            {
                new Coordinate(2, 2),
                new Coordinate(13, 2),
                new Coordinate(13, 13),
                new Coordinate(2, 13),
            };
            LastMobSpawnDateTime = DateTime.UtcNow;

            InitializeSounds();
            Goals = new List<Goal>()
            {
                new Goal("Тестовая цель", false)
            };
        }

        private List<Mob> InitializeMobs()
        {
            var mob = new Mob
            {
                Id = 1,
                MaxHealthValue = 100,
                HealthValue = 100,
                StepValue = 0.05,
                DamageCooldownMilliseconds = 500,
                DamageValue = 20,
                Coordinate = new Coordinate(2, 2),
                Texture = AppDataHelper.GetTexture("Mobs\\Kaka.png"),
                SoundDeath = new MediaPlayer()
            };
            mob.SoundDeath.Open(new Uri(Paths.GetMobsSoundAbsolutePath("death.wav")));

            var mob2 = new Mob
            {
                Id = 2,
                MaxHealthValue = 100,
                HealthValue = 100,
                StepValue = 0.05,
                DamageCooldownMilliseconds = 500,
                DamageValue = 20,
                Coordinate = new Coordinate(2, 5),
                Texture = AppDataHelper.GetTexture("Mobs\\Kaka.png"),
                SoundDeath = new MediaPlayer()
            };
            mob.SoundDeath.Open(new Uri(Paths.GetMobsSoundAbsolutePath("death.wav")));

            return new List<Mob>
            {
                //mob,
                //mob2
            };
        }

        private List<AIDKit> InitializeAIDKits()
        {
            return new List<AIDKit>
            {
                new AIDKit
                {
                    Coordinate = new Coordinate(0, 0),
                    Position = CollectableObjectPosition.OnMap,
                    RecoverableHealthValue = 50,
                    Texture = AppDataHelper.GetTexture("AIDKits\\Test.png")
                }
            };
        }

        private List<Weapon> InitializeWeapons()
        {
            var weaponGun = new Weapon
            {
                Coordinate = new Coordinate(0, 0),
                Position = CollectableObjectPosition.Player,
                Type = WeaponType.Gun,
                Title = Weapon.ObtainWeaponTitle(WeaponType.Gun),
                DamageValue = 10,
                DamageCooldown = 500,
                BulletsCount = 100,
                Texture = AppDataHelper.GetTexture("Weapons\\Gun.png"),
                ShotTexture = AppDataHelper.GetTexture("Weapons\\Shot.png"),
                SoundShoot = new MediaPlayer()
            };
            weaponGun.SoundShoot.Open(new Uri(Paths.GetWeaponsSoundAbsolutePath("gun.wav")));
            weaponGun.SoundShoot.MediaEnded += MediaPlayerReset;

            var weaponRifle = new Weapon
            {
                Coordinate = new Coordinate(0, 0),
                Position = CollectableObjectPosition.Player,
                Type = WeaponType.Rifle,
                Title = Weapon.ObtainWeaponTitle(WeaponType.Rifle),
                DamageValue = 30,
                DamageCooldown = 1000,
                BulletsCount = 100,
                Texture = AppDataHelper.GetTexture("Weapons\\Rifle.png"),
                ShotTexture = AppDataHelper.GetTexture("Weapons\\Shot.png"),
                SoundShoot = new MediaPlayer()
            };
            weaponRifle.SoundShoot.Open(new Uri(Paths.GetWeaponsSoundAbsolutePath("gun.wav")));
            weaponRifle.SoundShoot.MediaEnded += MediaPlayerReset;

            var weaponShotgun = new Weapon
            {
                Coordinate = new Coordinate(0, 0),
                Position = CollectableObjectPosition.Player,
                Type = WeaponType.ShotGun,
                Title = Weapon.ObtainWeaponTitle(WeaponType.ShotGun),
                DamageValue = 100,
                DamageCooldown = 1000,
                BulletsCount = 10,
                Texture = AppDataHelper.GetTexture("Weapons\\Shotgun.png"),
                ShotTexture = AppDataHelper.GetTexture("Weapons\\Shot.png"),
                SoundShoot = new MediaPlayer()
            };
            weaponShotgun.SoundShoot.Open(new Uri(Paths.GetWeaponsSoundAbsolutePath("shotgun.wav")));
            weaponShotgun.SoundShoot.MediaEnded += MediaPlayerReset;

            return new List<Weapon>
            {
                weaponGun,
                weaponRifle,
                weaponShotgun
            };
        }

        private void MediaPlayerReset(object? sender, EventArgs e)
        {
            var mediaPlayer = sender as MediaPlayer;
            mediaPlayer!.Stop();
            mediaPlayer!.Position = new TimeSpan(0);
        }

        private List<BulletsPack> InitializeBulletsPacks()
        {
            return new List<BulletsPack>
            {
                new BulletsPack
                {
                    Coordinate = new Coordinate(0, 0),
                    Count = 100,
                    WeaponType = WeaponType.Rifle,
                    Texture = AppDataHelper.GetTexture("BulletPacks\\Test.png")
                }
            };
        }

        private void InitializeSounds()
        {
            SoundBackground = new MediaPlayer();
            SoundBackground.Open(new Uri(Paths.GetBackgroundSoundAbsolutePath("level1.wav")));
        }

        #endregion

        public override void Start()
        {
            //BackGroundPlayer.Play();
        }

        public override void UpdateEntities()
        {
            if(IsShooting)
            {
                var viewLine = new Line(Player.Coordinate, Player.Direction);

                var mobDistance = double.MaxValue;
                var iMob = (Mob?)null;
                foreach (var mob in Mobs)
                {
                    if (Math.Abs(viewLine.GetValue(mob.Coordinate.X, mob.Coordinate.Y)) <= 0.1)
                    {
                        var mobAngle = Math.Atan2(mob.Coordinate.Y - Player.Coordinate.Y, mob.Coordinate.X - Player.Coordinate.X);
                        while (mobAngle - Player.Direction > Math.PI)
                            mobAngle -= 2 * Math.PI;
                        while (mobAngle - Player.Direction < -Math.PI)
                            mobAngle += 2 * Math.PI;

                        if (mobAngle >= Player.Direction - Player.ViewAngle / 2 && mobAngle <= Player.Direction + Player.ViewAngle / 2)
                        {
                            var distance = AlgebraHelper.GetLengthBeetwenPoints(Player.Coordinate, mob.Coordinate);
                            if (distance < mobDistance)
                            {
                                mobDistance = distance;
                                iMob = mob;
                            }
                        }
                    }
                }

                if(iMob != null)
                {
                    iMob.HealthValue -= Player.CurrentWeapon.DamageValue;
                    if (iMob.HealthValue <= 0)
                    {
                        Mobs.Remove(iMob);
                        iMob.SoundDeath.Play();
                    }
                }
            }

            foreach(var mob in Mobs)
            {
                var distanceFromMobToPlayer = AlgebraHelper.GetLengthBeetwenPoints(mob.Coordinate, Player.Coordinate);

                if(distanceFromMobToPlayer <= 5 && distanceFromMobToPlayer > 0.5)
                {
                    var mobPlayerSegment = new Segment(Player.Coordinate, mob.Coordinate);
                    var hasWallIntersection = false;
                    foreach(var wall in Walls)
                    {
                        if(wall.Segment.GetIntersectionPoint(mobPlayerSegment, out var intersectionPoint))
                        {
                            hasWallIntersection = true;
                            break;
                        }
                    }
                    if(!hasWallIntersection)
                    {
                        var mobAngle = Math.Atan2(Player.Coordinate.Y - mob.Coordinate.Y, Player.Coordinate.X - mob.Coordinate.X);
                        var deltaX = (float)(mob.StepValue * Math.Cos(mobAngle));
                        var deltaY = (float)(mob.StepValue * Math.Sin(mobAngle));
                        mob.Coordinate.X += deltaX;
                        mob.Coordinate.Y += deltaY;
                    }
                }

                if (distanceFromMobToPlayer <= 0.5)
                {
                    if(mob.LastShotDateTime == null || mob.LastShotDateTime.Value.AddMilliseconds(mob.DamageCooldownMilliseconds) < DateTime.UtcNow)
                    {
                        mob.LastShotDateTime = DateTime.UtcNow;
                        Player.HealthValue -= mob.DamageValue;
                        ApplicationForm.GamePlayerHealthLabel.Text = Player.HealthValue.ToString();
                    }
                }
            }

            //Спавн мобов
            if(LastMobSpawnDateTime.AddSeconds(10) < DateTime.UtcNow)
            {
                var randomIndex = new Random().Next(MobsSpawnPoints.Count() - 1);
                var spawnPoint = MobsSpawnPoints[randomIndex];
                var offset = new Random().NextDouble();

                var mob = new Mob
                {
                    Id = Mobs.Count() == 0 ? 1 : Mobs.Max(x => x.Id) + 1,
                    MaxHealthValue = 100,
                    HealthValue = 100,
                    StepValue = 0.05,
                    DamageCooldownMilliseconds = 500,
                    DamageValue = 10,
                    Coordinate = new Coordinate(spawnPoint.X + offset, spawnPoint.Y + offset),
                    Texture = AppDataHelper.GetTexture("Mobs\\Kaka.png"),
                    SoundDeath = new MediaPlayer()
                };
                mob.SoundDeath.Open(new Uri(Paths.GetMobsSoundAbsolutePath("death.wav")));
                Mobs.Add(mob);

                randomIndex = new Random().Next(MobsSpawnPoints.Count() - 1);
                spawnPoint = MobsSpawnPoints[randomIndex];
                offset = new Random().NextDouble();

                var mob2 = new Mob
                {
                    Id = Mobs.Count() == 0 ? 1 : Mobs.Max(x => x.Id) + 1,
                    MaxHealthValue = 100,
                    HealthValue = 100,
                    StepValue = 0.05,
                    DamageCooldownMilliseconds = 500,
                    DamageValue = 10,
                    Coordinate = new Coordinate(spawnPoint.X + offset, spawnPoint.Y + offset),
                    Texture = AppDataHelper.GetTexture("Mobs\\Kaka.png"),
                    SoundDeath = new MediaPlayer()
                };
                mob2.SoundDeath.Open(new Uri(Paths.GetMobsSoundAbsolutePath("death.wav")));
                Mobs.Add(mob2);

                randomIndex = new Random().Next(MobsSpawnPoints.Count() - 1);
                spawnPoint = MobsSpawnPoints[randomIndex];
                offset = new Random().NextDouble();

                var mob3 = new Mob
                {
                    Id = Mobs.Count() == 0 ? 1 : Mobs.Max(x => x.Id) + 1,
                    MaxHealthValue = 100,
                    HealthValue = 100,
                    StepValue = 0.05,
                    DamageCooldownMilliseconds = 500,
                    DamageValue = 10,
                    Coordinate = new Coordinate(spawnPoint.X + offset, spawnPoint.Y + offset),
                    Texture = AppDataHelper.GetTexture("Mobs\\Kaka.png"),
                    SoundDeath = new MediaPlayer()
                };
                mob3.SoundDeath.Open(new Uri(Paths.GetMobsSoundAbsolutePath("death.wav")));
                Mobs.Add(mob3);

                randomIndex = new Random().Next(MobsSpawnPoints.Count() - 1);
                spawnPoint = MobsSpawnPoints[randomIndex];
                offset = new Random().NextDouble();

                var mob4 = new Mob
                {
                    Id = Mobs.Count() == 0 ? 1 : Mobs.Max(x => x.Id) + 1,
                    MaxHealthValue = 100,
                    HealthValue = 100,
                    StepValue = 0.05,
                    DamageCooldownMilliseconds = 500,
                    DamageValue = 10,
                    Coordinate = new Coordinate(spawnPoint.X + offset, spawnPoint.Y + offset),
                    Texture = AppDataHelper.GetTexture("Mobs\\Kaka.png"),
                    SoundDeath = new MediaPlayer()
                };
                mob4.SoundDeath.Open(new Uri(Paths.GetMobsSoundAbsolutePath("death.wav")));
                Mobs.Add(mob4);

                LastMobSpawnDateTime = DateTime.UtcNow;
            }

            MovePlayer(Walls);
            RotateCamera();
        }

        private void MovePlayer(List<Wall> walls)
        {
            float deltaX = 0;
            float deltaY = 0;

            if (MovingPlayerForward)
            {
                var playerDirection = Player.Direction;
                var directionRay = new Ray(Player.Coordinate, playerDirection);
                var hasWallIntersection = walls
                    .Any(x => directionRay.GetIntersectionPoint(x.Segment, out var intersectionPoint, out var d) &&
                              AlgebraHelper.GetLengthBeetwenPoints(intersectionPoint, Player.Coordinate) <= Player.StepValue);

                if (!hasWallIntersection)
                {
                    deltaX += (float)(Player.StepValue * Math.Cos(Player.Direction));
                    deltaY += (float)(Player.StepValue * Math.Sin(Player.Direction));
                }
            }
            if (MovingPlayerBackward)
            {
                var playerDirection = Player.Direction + Math.PI;
                var directionRay = new Ray(Player.Coordinate, playerDirection);
                var hasWallIntersection = walls
                    .Any(x => directionRay.GetIntersectionPoint(x.Segment, out var intersectionPoint, out var d) &&
                              AlgebraHelper.GetLengthBeetwenPoints(intersectionPoint, Player.Coordinate) <= Player.StepValue);

                if (!hasWallIntersection)
                {
                    deltaX += (float)(Player.StepValue * Math.Cos(playerDirection));
                    deltaY += (float)(Player.StepValue * Math.Sin(playerDirection));
                }
            }
            if (MovingPlayerLeft)
            {
                var playerDirection = Player.Direction - Math.PI / 2;
                var directionRay = new Ray(Player.Coordinate, playerDirection);
                var hasWallIntersection = walls
                    .Any(x => directionRay.GetIntersectionPoint(x.Segment, out var intersectionPoint, out var d) &&
                              AlgebraHelper.GetLengthBeetwenPoints(intersectionPoint, Player.Coordinate) <= Player.StepValue);

                if (!hasWallIntersection)
                {
                    deltaX += (float)(Player.StepValue * Math.Cos(playerDirection));
                    deltaY += (float)(Player.StepValue * Math.Sin(playerDirection));
                }
            }
            if (MovingPlayerRight)
            {
                var playerDirection = Player.Direction + Math.PI / 2;
                var directionRay = new Ray(Player.Coordinate, playerDirection);
                var hasWallIntersection = walls
                    .Any(x => directionRay.GetIntersectionPoint(x.Segment, out var intersectionPoint, out var d) &&
                              AlgebraHelper.GetLengthBeetwenPoints(intersectionPoint, Player.Coordinate) <= Player.StepValue);

                if (!hasWallIntersection)
                {
                    deltaX += (float)(Player.StepValue * Math.Cos(playerDirection));
                    deltaY += (float)(Player.StepValue * Math.Sin(playerDirection));
                }
            }

            Player.Coordinate.X += deltaX;
            Player.Coordinate.Y += deltaY;
        }

        private void RotateCamera()
        {
            if (RotateCameraLeft)
            {
                Player.Direction -= Player.StepCamera;
            }
            if (RotateCameraRight)
            {
                Player.Direction += Player.StepCamera;
            }
        }

        public override bool SetWeapon(int index, out int ammo)
        {
            if (index < 0 || Player.Weapons.Count - 1 < index)
            {
                ammo = 0;
                return false;
            }

            Player.CurrentWeapon = Player.Weapons[index];
            ammo = Player.CurrentWeapon.BulletsCount;
            return true;
        }

        public override bool PlayerShot(out int ammo)
        {
            if(Player.CurrentWeapon.BulletsCount > 0)
            {
                Player.CurrentWeapon.BulletsCount--;
                IsShooting = true;
                ammo = Player.CurrentWeapon.BulletsCount;

                Player.CurrentWeapon.SoundShoot.Stop();
                Player.CurrentWeapon.SoundShoot.Position = new TimeSpan(0);
                Player.CurrentWeapon.SoundShoot.Play();
                
                return true;
            }

            ammo = 0;
            return false;
        }

        public override void Complete()
        {
            throw new NotImplementedException();
        }

        public override void Over()
        {
            throw new NotImplementedException();
        }
    }
}
