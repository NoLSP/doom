﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOOM
{
    public class Goal
    {
        public string Title { get; set; }
        public bool IsComplete { get; set; }

        public Goal(string title, bool isComplete) 
        {
            Title = title;
            IsComplete = isComplete;
        }
    }
}
