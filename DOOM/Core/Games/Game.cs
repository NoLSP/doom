﻿using DOOM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using System.Text;
using System.Threading.Tasks;

namespace DOOM
{
    public abstract class Game
    {
        public int Id { get; set; }
        public string Title { get; set; } //название
        public string Description { get; set; } // описание
        public List<Wall> Walls;
        public List<Mob> Mobs;
        public List<AIDKit> AIDKits;
        public List<Weapon> Weapons;
        public List<BulletsPack> BulletsPacks;
        public Player Player;
        public List<Goal> Goals;
        public bool IsGameOver;

        public MediaPlayer SoundBackground;
        public MediaPlayer SoundGameOver;
        public MediaPlayer SoundGameComplete;

        public bool MovingPlayerForward = false;
        public bool MovingPlayerBackward = false;
        public bool MovingPlayerLeft = false;
        public bool MovingPlayerRight = false;

        public bool RotateCameraRight = false;
        public bool RotateCameraLeft = false;

        public bool IsShooting = false;

        public abstract void Start();
        public abstract void UpdateEntities();
        public abstract bool SetWeapon(int index, out int ammo);
        public abstract bool PlayerShot(out int ammo);
        public abstract void Complete();
        public abstract void Over();
    }
}
