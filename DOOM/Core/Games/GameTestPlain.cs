﻿using Algebra;
using DOOM.Constants;
using DOOM.Helpers;
using DOOM.Models;
using Doom3D.Algebra;
using System.Media;
using System.Numerics;
using System.Security.Policy;
using System.Windows.Media;

namespace DOOM
{
    public class GameTestPlain : Game
    {
        private List<Coordinate> MobsSpawnPoints;
        private DateTime LastMobSpawnDateTime;

        private int KilledMobsGoalCount = 20;
        private int KilledMobsCount = 0;

        #region Initialization

        public GameTestPlain(List<Wall> walls) 
        {
            Id = 2;
            Title = "Тестовый пустой уровень";
            Description = "Уровень для тестов";
            Walls = walls;
            MobsSpawnPoints = new List<Coordinate>()
            {
                new Coordinate(2, 2),
                new Coordinate(13, 2),
                new Coordinate(13, 13),
                new Coordinate(2, 13),
            };
            LastMobSpawnDateTime = DateTime.UtcNow;
            Mobs = InitializeMobs();
            AIDKits = InitializeAIDKits();
            Weapons = InitializeWeapons();
            var playerWeapons = Weapons.Where(x => x.Position == CollectableObjectPosition.Player).ToList();
            BulletsPacks = InitializeBulletsPacks();
            Player = new Player
            {
                Direction = -Math.PI,
                ViewAngle = Math.PI / 3,
                StepValue = 0.2,
                StepCamera = Math.PI / 40,
                Coordinate = new Coordinate(7, 7),
                HealthValue = 100,
                MaxHealthValue = 100,
                AIDkits = new List<AIDKit>(),
                Weapons = playerWeapons,
                CurrentWeapon = playerWeapons[0],
                SoundDeath = new MediaPlayer()
            };
            Player.SoundDeath.Open(new Uri(Paths.GetPlayerSoundAbsolutePath("death.wav")));
            Player.SoundDeath.Volume = 1;

            InitializeSounds();
            Goals = new List<Goal>()
            {
                new Goal($"Убито мобов - {KilledMobsCount}/{KilledMobsGoalCount}", false)
            };
        }

        private List<Mob> InitializeMobs()
        {
            var mob1 = new Mob
            {
                Id = 1,
                MaxHealthValue = 100,
                HealthValue = 100,
                StepValue = 0.05,
                DamageCooldownMilliseconds = 1000,
                DamageValue = 10,
                Coordinate = new Coordinate(MobsSpawnPoints[0].X, MobsSpawnPoints[0].Y),
                Texture = AppDataHelper.GetTexture("Mobs\\Kaka.png"),
                SoundDeath = new MediaPlayer()
            };
            mob1.SoundDeath.Open(new Uri(Paths.GetMobsSoundAbsolutePath("death.wav")));

            var mob2 = new Mob
            {
                Id = 2,
                MaxHealthValue = 100,
                HealthValue = 100,
                StepValue = 0.05,
                DamageCooldownMilliseconds = 1000,
                DamageValue = 10,
                Coordinate = new Coordinate(MobsSpawnPoints[1].X, MobsSpawnPoints[1].Y),
                Texture = AppDataHelper.GetTexture("Mobs\\Kaka.png"),
                SoundDeath = new MediaPlayer()
            };
            mob2.SoundDeath.Open(new Uri(Paths.GetMobsSoundAbsolutePath("death.wav")));

            var mob3 = new Mob
            {
                Id = 3,
                MaxHealthValue = 100,
                HealthValue = 100,
                StepValue = 0.05,
                DamageCooldownMilliseconds = 1000,
                DamageValue = 10,
                Coordinate = new Coordinate(MobsSpawnPoints[2].X, MobsSpawnPoints[2].Y),
                Texture = AppDataHelper.GetTexture("Mobs\\Kaka.png"),
                SoundDeath = new MediaPlayer()
            };
            mob3.SoundDeath.Open(new Uri(Paths.GetMobsSoundAbsolutePath("death.wav")));

            var mob4 = new Mob
            {
                Id = 4,
                MaxHealthValue = 100,
                HealthValue = 100,
                StepValue = 0.05,
                DamageCooldownMilliseconds = 1000,
                DamageValue = 10,
                Coordinate = new Coordinate(MobsSpawnPoints[3].X, MobsSpawnPoints[3].Y),
                Texture = AppDataHelper.GetTexture("Mobs\\Kaka.png"),
                SoundDeath = new MediaPlayer()
            };
            mob4.SoundDeath.Open(new Uri(Paths.GetMobsSoundAbsolutePath("death.wav")));
            
            return new List<Mob>
            {
                mob1,
                mob2,
                mob3,
                mob4
            };
        }

        private List<AIDKit> InitializeAIDKits()
        {
            return new List<AIDKit>
            {
                new AIDKit
                {
                    Coordinate = new Coordinate(0, 0),
                    Position = CollectableObjectPosition.OnMap,
                    RecoverableHealthValue = 50,
                    Texture = AppDataHelper.GetTexture("AIDKits\\Test.png")
                }
            };
        }

        private List<Weapon> InitializeWeapons()
        {
            var weaponGun = new Weapon
            {
                Coordinate = new Coordinate(0, 0),
                Position = CollectableObjectPosition.Player,
                Type = WeaponType.Gun,
                Title = Weapon.ObtainWeaponTitle(WeaponType.Gun),
                DamageValue = 10,
                DamageCooldown = 500,
                BulletsCount = 100,
                Texture = AppDataHelper.GetTexture("Weapons\\Gun.png"),
                ShotTexture = AppDataHelper.GetTexture("Weapons\\Shot.png"),
                SoundShoot = new MediaPlayer()
            };
            weaponGun.SoundShoot.Open(new Uri(Paths.GetWeaponsSoundAbsolutePath("gun.wav")));
            weaponGun.SoundShoot.MediaEnded += MediaPlayerReset;

            var weaponRifle = new Weapon
            {
                Coordinate = new Coordinate(0, 0),
                Position = CollectableObjectPosition.Player,
                Type = WeaponType.Rifle,
                Title = Weapon.ObtainWeaponTitle(WeaponType.Rifle),
                DamageValue = 35,
                DamageCooldown = 1000,
                BulletsCount = 20,
                Texture = AppDataHelper.GetTexture("Weapons\\Rifle.png"),
                ShotTexture = AppDataHelper.GetTexture("Weapons\\Shot.png"),
                SoundShoot = new MediaPlayer()
            };
            weaponRifle.SoundShoot.Open(new Uri(Paths.GetWeaponsSoundAbsolutePath("gun.wav")));
            weaponRifle.SoundShoot.MediaEnded += MediaPlayerReset;

            var weaponShotgun = new Weapon
            {
                Coordinate = new Coordinate(0, 0),
                Position = CollectableObjectPosition.Player,
                Type = WeaponType.ShotGun,
                Title = Weapon.ObtainWeaponTitle(WeaponType.ShotGun),
                DamageValue = 100,
                DamageCooldown = 1000,
                BulletsCount = 10,
                Texture = AppDataHelper.GetTexture("Weapons\\Shotgun.png"),
                ShotTexture = AppDataHelper.GetTexture("Weapons\\Shot.png"),
                SoundShoot = new MediaPlayer()
            };
            weaponShotgun.SoundShoot.Open(new Uri(Paths.GetWeaponsSoundAbsolutePath("shotgun.wav")));
            weaponShotgun.SoundShoot.MediaEnded += MediaPlayerReset;

            return new List<Weapon>
            {
                weaponGun,
                weaponRifle,
                weaponShotgun
            };
        }

        private void MediaPlayerReset(object? sender, EventArgs e)
        {
            var mediaPlayer = sender as MediaPlayer;
            mediaPlayer!.Stop();
            mediaPlayer!.Position = new TimeSpan(0);
        }

        private List<BulletsPack> InitializeBulletsPacks()
        {
            return new List<BulletsPack>
            {
                new BulletsPack
                {
                    Coordinate = new Coordinate(0, 0),
                    Count = 100,
                    WeaponType = WeaponType.Rifle,
                    Texture = AppDataHelper.GetTexture("BulletPacks\\Test.png")
                }
            };
        }

        private void InitializeSounds()
        {
            SoundBackground = new MediaPlayer();
            SoundBackground.Open(new Uri(Paths.GetBackgroundSoundAbsolutePath("level1.wav")));

            SoundGameOver = new MediaPlayer();
            SoundGameOver.Open(new Uri(Paths.GetSoundAbsolutePath("game_over.wav")));

            SoundGameComplete = new MediaPlayer();
            SoundGameComplete.Open(new Uri(Paths.GetSoundAbsolutePath("game_complete.wav")));
            SoundGameComplete.Volume = 1;
        }

        #endregion

        public override void Start()
        {
            SoundBackground.Play();
        }

        public override void UpdateEntities()
        {
            if(IsShooting)
            {
                var viewLine = new Line(Player.Coordinate, Player.Direction);

                var mobDistance = double.MaxValue;
                var iMob = (Mob?)null;
                foreach (var mob in Mobs)
                {
                    if (Math.Abs(viewLine.GetValue(mob.Coordinate.X, mob.Coordinate.Y)) <= 0.1)
                    {
                        var mobAngle = Math.Atan2(mob.Coordinate.Y - Player.Coordinate.Y, mob.Coordinate.X - Player.Coordinate.X);
                        while (mobAngle - Player.Direction > Math.PI)
                            mobAngle -= 2 * Math.PI;
                        while (mobAngle - Player.Direction < -Math.PI)
                            mobAngle += 2 * Math.PI;

                        if (mobAngle >= Player.Direction - Player.ViewAngle / 2 && mobAngle <= Player.Direction + Player.ViewAngle / 2)
                        {
                            var distance = AlgebraHelper.GetLengthBeetwenPoints(Player.Coordinate, mob.Coordinate);
                            if (distance < mobDistance)
                            {
                                mobDistance = distance;
                                iMob = mob;
                            }
                        }
                    }
                }

                if(iMob != null)
                {
                    iMob.HealthValue -= Player.CurrentWeapon.DamageValue;
                    if (iMob.HealthValue <= 0)
                    {
                        Mobs.Remove(iMob);
                        iMob.SoundDeath.Play();
                        KilledMobsCount++;
                    }
                }
            }

            foreach(var mob in Mobs)
            {
                var distanceFromMobToPlayer = AlgebraHelper.GetLengthBeetwenPoints(mob.Coordinate, Player.Coordinate);

                if (distanceFromMobToPlayer <= 0.5)
                {
                    if (mob.LastShotDateTime == null || mob.LastShotDateTime.Value.AddMilliseconds(mob.DamageCooldownMilliseconds) < DateTime.UtcNow)
                    {
                        mob.LastShotDateTime = DateTime.UtcNow;
                        Player.HealthValue -= mob.DamageValue;
                        ApplicationForm.GamePlayerHealthLabel.Text = Player.HealthValue.ToString();
                        Player.SoundDeath.Position = TimeSpan.FromMilliseconds(0);
                        Player.SoundDeath.Play();
                        if (Player.HealthValue <= 0)
                        {
                            IsGameOver = true;
                        }
                    }
                }
                else
                {
                    var mobAngle = Math.Atan2(Player.Coordinate.Y - mob.Coordinate.Y, Player.Coordinate.X - mob.Coordinate.X);
                    var deltaX = mob.StepValue * Math.Cos(mobAngle);
                    var deltaY = mob.StepValue * Math.Sin(mobAngle);
                    mob.Coordinate.X += deltaX;
                    mob.Coordinate.Y += deltaY;
                }
            }

            //Спавн мобов
            if(LastMobSpawnDateTime.AddSeconds(20) < DateTime.UtcNow)
            {
                var mob = new Mob
                {
                    Id = Mobs.Count() == 0 ? 1 : Mobs.Max(x => x.Id) + 1,
                    MaxHealthValue = 100,
                    HealthValue = 100,
                    StepValue = 0.05,
                    DamageCooldownMilliseconds = 1000,
                    DamageValue = 10,
                    Coordinate = new Coordinate(MobsSpawnPoints[0].X, MobsSpawnPoints[0].Y),
                    Texture = AppDataHelper.GetTexture("Mobs\\Kaka.png"),
                    SoundDeath = new MediaPlayer()
                };
                mob.SoundDeath.Open(new Uri(Paths.GetMobsSoundAbsolutePath("death.wav")));
                Mobs.Add(mob);

                var mob2 = new Mob
                {
                    Id = Mobs.Count() == 0 ? 1 : Mobs.Max(x => x.Id) + 1,
                    MaxHealthValue = 100,
                    HealthValue = 100,
                    StepValue = 0.05,
                    DamageCooldownMilliseconds = 1000,
                    DamageValue = 10,
                    Coordinate = new Coordinate(MobsSpawnPoints[1].X, MobsSpawnPoints[1].Y),
                    Texture = AppDataHelper.GetTexture("Mobs\\Kaka.png"),
                    SoundDeath = new MediaPlayer()
                };
                mob2.SoundDeath.Open(new Uri(Paths.GetMobsSoundAbsolutePath("death.wav")));
                Mobs.Add(mob2);

                var mob3 = new Mob
                {
                    Id = Mobs.Count() == 0 ? 1 : Mobs.Max(x => x.Id) + 1,
                    MaxHealthValue = 100,
                    HealthValue = 100,
                    StepValue = 0.05,
                    DamageCooldownMilliseconds = 1000,
                    DamageValue = 10,
                    Coordinate = new Coordinate(MobsSpawnPoints[2].X, MobsSpawnPoints[2].Y),
                    Texture = AppDataHelper.GetTexture("Mobs\\Kaka.png"),
                    SoundDeath = new MediaPlayer()
                };
                mob3.SoundDeath.Open(new Uri(Paths.GetMobsSoundAbsolutePath("death.wav")));
                Mobs.Add(mob3);

                var mob4 = new Mob
                {
                    Id = Mobs.Count() == 0 ? 1 : Mobs.Max(x => x.Id) + 1,
                    MaxHealthValue = 100,
                    HealthValue = 100,
                    StepValue = 0.05,
                    DamageCooldownMilliseconds = 1000,
                    DamageValue = 10,
                    Coordinate = new Coordinate(MobsSpawnPoints[3].X, MobsSpawnPoints[3].Y),
                    Texture = AppDataHelper.GetTexture("Mobs\\Kaka.png"),
                    SoundDeath = new MediaPlayer()
                };
                mob4.SoundDeath.Open(new Uri(Paths.GetMobsSoundAbsolutePath("death.wav")));
                Mobs.Add(mob4);

                LastMobSpawnDateTime = DateTime.UtcNow;
            }

            MovePlayer(Walls);
            RotateCamera();

            Goals = new List<Goal>()
            {
                new Goal($"Убито мобов - {KilledMobsCount}/{KilledMobsGoalCount}", KilledMobsCount == KilledMobsGoalCount)
            };
        }

        private void MovePlayer(List<Wall> walls)
        {
            float deltaX = 0;
            float deltaY = 0;

            if (MovingPlayerForward)
            {
                var playerDirection = Player.Direction;
                var directionRay = new Ray(Player.Coordinate, playerDirection);
                var hasWallIntersection = walls
                    .Any(x => directionRay.GetIntersectionPoint(x.Segment, out var intersectionPoint, out var d) &&
                              AlgebraHelper.GetLengthBeetwenPoints(intersectionPoint, Player.Coordinate) <= Player.StepValue);

                if (!hasWallIntersection)
                {
                    deltaX += (float)(Player.StepValue * Math.Cos(Player.Direction));
                    deltaY += (float)(Player.StepValue * Math.Sin(Player.Direction));
                }
            }
            if (MovingPlayerBackward)
            {
                var playerDirection = Player.Direction + Math.PI;
                var directionRay = new Ray(Player.Coordinate, playerDirection);
                var hasWallIntersection = walls
                    .Any(x => directionRay.GetIntersectionPoint(x.Segment, out var intersectionPoint, out var d) &&
                              AlgebraHelper.GetLengthBeetwenPoints(intersectionPoint, Player.Coordinate) <= Player.StepValue);

                if (!hasWallIntersection)
                {
                    deltaX += (float)(Player.StepValue * Math.Cos(playerDirection));
                    deltaY += (float)(Player.StepValue * Math.Sin(playerDirection));
                }
            }
            if (MovingPlayerLeft)
            {
                var playerDirection = Player.Direction - Math.PI / 2;
                var directionRay = new Ray(Player.Coordinate, playerDirection);
                var hasWallIntersection = walls
                    .Any(x => directionRay.GetIntersectionPoint(x.Segment, out var intersectionPoint, out var d) &&
                              AlgebraHelper.GetLengthBeetwenPoints(intersectionPoint, Player.Coordinate) <= Player.StepValue);

                if (!hasWallIntersection)
                {
                    deltaX += (float)(Player.StepValue * Math.Cos(playerDirection));
                    deltaY += (float)(Player.StepValue * Math.Sin(playerDirection));
                }
            }
            if (MovingPlayerRight)
            {
                var playerDirection = Player.Direction + Math.PI / 2;
                var directionRay = new Ray(Player.Coordinate, playerDirection);
                var hasWallIntersection = walls
                    .Any(x => directionRay.GetIntersectionPoint(x.Segment, out var intersectionPoint, out var d) &&
                              AlgebraHelper.GetLengthBeetwenPoints(intersectionPoint, Player.Coordinate) <= Player.StepValue);

                if (!hasWallIntersection)
                {
                    deltaX += (float)(Player.StepValue * Math.Cos(playerDirection));
                    deltaY += (float)(Player.StepValue * Math.Sin(playerDirection));
                }
            }

            Player.Coordinate.X += deltaX;
            Player.Coordinate.Y += deltaY;
        }

        private void RotateCamera()
        {
            if (RotateCameraLeft)
            {
                Player.Direction -= Player.StepCamera;
            }
            if (RotateCameraRight)
            {
                Player.Direction += Player.StepCamera;
            }
        }

        public override bool SetWeapon(int index, out int ammo)
        {
            if (index < 0 || Player.Weapons.Count - 1 < index)
            {
                ammo = 0;
                return false;
            }

            Player.CurrentWeapon = Player.Weapons[index];
            ammo = Player.CurrentWeapon.BulletsCount;
            return true;
        }

        public override bool PlayerShot(out int ammo)
        {
            if(Player.CurrentWeapon.BulletsCount > 0)
            {
                Player.CurrentWeapon.BulletsCount--;
                IsShooting = true;
                ammo = Player.CurrentWeapon.BulletsCount;

                Player.CurrentWeapon.SoundShoot.Stop();
                Player.CurrentWeapon.SoundShoot.Position = new TimeSpan(0);
                Player.CurrentWeapon.SoundShoot.Play();
                
                return true;
            }

            ammo = 0;
            return false;
        }

        public override void Complete()
        {
            SoundBackground.Stop();
            SoundGameComplete.Play();
        }

        public override void Over()
        {
            SoundBackground.Stop();
            SoundGameOver.Play();
        }
    }
}
