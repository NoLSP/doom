﻿using Algebra;
using DOOM.Helpers;
using DOOM.Models;
using Doom3D.Algebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Reflection.Metadata;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace DOOM
{
    public static class DrawingSystem
    {
        public static Bitmap GetFrame(Game game)
        {
            Bitmap gameBitmap = new Bitmap(1000, 650); // Создаем картинку
            Graphics gameGarphics = Graphics.FromImage(gameBitmap); // Создаем графику

            var playerLocation = game.Player.Coordinate;
            var playerDirection = game.Player.Direction; // Направление угла обозора
            var playerViewAngle = game.Player.ViewAngle;

            var mobInfos = new List<MobInfo>();

            for (int i = 0; i < 1000; i++)
            {
                //куда смотрим
                var angle = playerDirection - playerViewAngle / 2 + playerViewAngle * i / 1000;

                //Луч
                var viewRay = new Ray(playerLocation, angle);

                //Ищем ближайшую точку пересечения со стеной
                var wallIntersectionPoint = (Coordinate?)null;
                var wallIntersectionPointDistance = double.MaxValue;
                var wallDistanceFromSegmentStartToIntersectionPoint = double.MaxValue;
                var iWall = (Wall?)null;
                foreach(var wall in game.Walls)
                {
                    if(viewRay.GetIntersectionPoint(wall.Segment, out var intersectionPoint, out var distanceFromSegmentStartToIntersectionPoint))
                    {
                        var distance = AlgebraHelper.GetLengthBeetwenPoints(playerLocation, intersectionPoint!);
                        if(distance < wallIntersectionPointDistance)
                        {
                            wallIntersectionPoint = intersectionPoint;
                            wallIntersectionPointDistance = distance;
                            wallDistanceFromSegmentStartToIntersectionPoint = distanceFromSegmentStartToIntersectionPoint!.Value;
                            iWall = wall;
                        }
                    }
                }

                //Если нашли стену
                if (iWall != null)
                {
                    var textureIndex = (int)Math.Round((wallDistanceFromSegmentStartToIntersectionPoint * (iWall.Texture.Width / iWall!.Segment.Length)));
                    var textureColumn = iWall!.TextureParts[textureIndex >= iWall!.TextureParts.Count() ? iWall!.TextureParts.Count() - 1 : textureIndex];

                    var wallHeight = (int)(650 / (wallIntersectionPointDistance * Math.Cos(angle - playerDirection)));
                    gameGarphics.DrawImage(textureColumn, new Rectangle(i, (650 - wallHeight) / 2, 1, wallHeight),
                                    new Rectangle(0, 0, 1, 64), GraphicsUnit.Pixel);
                }

                var viewLine = new Line(playerLocation, angle);

                var mobDistance = double.MaxValue;
                var iMob = (Mob?)null;
                foreach (var mob in game.Mobs)
                {
                    if (Math.Abs(viewLine.GetValue(mob.Coordinate.X, mob.Coordinate.Y)) <= 0.01)
                    {
                        var mobAngle = Math.Atan2(mob.Coordinate.Y - playerLocation.Y, mob.Coordinate.X - playerLocation.X);
                        while (mobAngle - playerDirection > Math.PI) 
                            mobAngle -= 2 * Math.PI;
                        while (mobAngle - playerDirection < -Math.PI) 
                            mobAngle += 2 * Math.PI;    
                        
                        if (mobAngle >= playerDirection - playerViewAngle / 2 && mobAngle <= playerDirection + playerViewAngle / 2)
                        {
                            var distance = AlgebraHelper.GetLengthBeetwenPoints(playerLocation, mob.Coordinate);
                            if (distance < mobDistance)
                            {
                                mobDistance = distance;
                                iMob = mob;
                            }
                        }
                    }
                }

                if (iMob != null && mobDistance < wallIntersectionPointDistance && !mobInfos.Any(x => x.Mob.Id == iMob.Id))
                {
                    mobInfos.Add(new MobInfo() 
                    { 
                        ColumnNumber = i, 
                        Distance = mobDistance,
                        Mob = iMob!
                    });
                }
            }

            foreach (var mobInfo in mobInfos.OrderByDescending(x => x.Distance))
            {
                var monsterHeight = (int)(mobInfo.Mob.Texture.Height * 2.5 / mobInfo.Distance);
                var scale = (double)monsterHeight / mobInfo.Mob.Texture.Height;
                var mobWidth = (int)(mobInfo.Mob.Texture.Width * scale);
                gameGarphics.DrawImage(mobInfo.Mob.Texture, new Rectangle(mobInfo.ColumnNumber - mobWidth / 2, (650 - monsterHeight) / 2,
                    mobWidth, monsterHeight), new Rectangle(0, 0, 124, 128), GraphicsUnit.Pixel);
                //128 на 134 - размер текстуры моба
            }

            if (game.IsShooting)
            {
                var shotTexture = game.Player.CurrentWeapon.ShotTexture;
                gameGarphics.DrawImage(shotTexture, new Point((1000 - shotTexture.Width) / 2 + 15, 650 - shotTexture.Height - 64));
                game.IsShooting = false;
            }

            var weapon = game.Player.CurrentWeapon.Texture;
            gameGarphics.DrawImage(weapon, new Point((1000 - weapon.Width) / 2, 650 - weapon.Height));

            return gameBitmap;
        }

    }
}
