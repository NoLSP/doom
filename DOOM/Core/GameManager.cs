﻿using DOOM.Helpers;
using DOOM.Models;
using Doom3D.Algebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace DOOM
{
    public static class GameManager
    {
        public static List<Game>? Games
        { 
            get; 
            private set; 
        }
        private static Game? CurrentGame;

        public static void SetGame(int gameId)
        {
            CurrentGame = Games.FirstOrDefault(x => x.Id == gameId);
            CurrentGame.Start();
        }

        public static Bitmap GetFrame()
        {
            return DrawingSystem.GetFrame(CurrentGame!);
        }

        public static void GameComplete()
        {
            CurrentGame.Complete();
        }

        public static void GameOver()
        {
            CurrentGame.Over();
        }

        public static bool SetWeapon(int index, out int ammo)
        {
            return CurrentGame!.SetWeapon(index, out ammo);
        }

        public static bool PlayerShot(out int ammo)
        {
            return CurrentGame!.PlayerShot(out ammo);
        }

        public static List<Goal> GetGameGoals()
        {
            return CurrentGame.Goals;
        }

        public static bool IsGameOver()
        {
            return CurrentGame.IsGameOver;
        }

        #region Moving Player

        public static void MovingPlayerForwardOn()
        {
            CurrentGame.MovingPlayerForward = true;
        }

        public static void MovingPlayerBackwardOn()
        {
            CurrentGame.MovingPlayerBackward = true;
        }

        public static void MovingPlayerLeftOn()
        {
            CurrentGame.MovingPlayerLeft = true;
        }

        public static void MovingPlayerRightOn()
        {
            CurrentGame.MovingPlayerRight = true;
        }

        public static void MovingPlayerForwardOff()
        {
            CurrentGame.MovingPlayerForward = false;
        }

        public static void MovingPlayerBackwardOff()
        {
            CurrentGame.MovingPlayerBackward = false;
        }

        public static void MovingPlayerLeftOff()
        {
            CurrentGame.MovingPlayerLeft = false;
        }

        public static void MovingPlayerRightOff()
        {
            CurrentGame.MovingPlayerRight = false;
        }

        public static void RotatePlayerLeftOn()
        {
            CurrentGame.RotateCameraLeft = true;
        }

        public static void RotatePlayerLeftOff()
        {
            CurrentGame.RotateCameraLeft = false;
        }

        public static void RotateCameraRightOn()
        {
            CurrentGame.RotateCameraRight = true;
        }

        public static void RotateCameraRightOff()
        {
            CurrentGame.RotateCameraRight = false;
        }

        #endregion

        public static void UpdateEntities()
        {
            CurrentGame.UpdateEntities();
        }

        public static void LoadLevels()
        {
            var gameTestwallInfos = AppDataHelper.GetLevelWallInfos("Test.json");
            var gameTestwalls = LoadWalls(gameTestwallInfos);

            var gameTestPlainwallInfos = AppDataHelper.GetLevelWallInfos("Test_Plain.json");
            var gameTestPlainwalls = LoadWalls(gameTestPlainwallInfos);

            Games = new List<Game>
            {
                new GameTest(gameTestwalls),
                new GameTestPlain(gameTestPlainwalls),
            };
        }

        private static List<Wall> LoadWalls(List<WallInfo> wallInfos)
        {
            var result = new List<Wall>();

            foreach (var wallInfo in wallInfos)
            {
                if (wallInfo.StartCoordinate == null)
                    throw new Exception("Начальная координата стены была null");

                if (wallInfo.EndCoordinate == null)
                    throw new Exception("Конечная координата стены была null");

                if (wallInfo.TextureRelativePath == null)
                    throw new Exception("Путь к текстуре стены был null");

                result.Add(new Wall(new Segment(wallInfo.StartCoordinate, wallInfo.EndCoordinate), AppDataHelper.GetTexture(wallInfo.TextureRelativePath)));
            }

            return result;
        }
    }
}
